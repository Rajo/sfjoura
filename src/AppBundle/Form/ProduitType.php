<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class ProduitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('code')
            ->add('titre')
            ->add('description')
            ->add('marque', EntityType::class, array(
                'class' => 'AppBundle:Marque',
                'choice_label' => 'nomMarque',
                'invalid_message'=>'Selectionnez un marque',
            ))
            ->add('qte')
            ->add('prixTTC')
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    'soleil' => 'Soleil',
                    'vue' => 'Vue',
                    'sport' => 'Sport',
                )))
            ->add('genre', ChoiceType::class, array(
                'choices'  => array(
                    'mixte' => 'Mixte',
                    'homme' => 'Homme',
                    'femme' => 'Femme',
                    'enfant' => 'Enfant',
                )));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Produit'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_produit';
    }


}
