<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('prenomClient')->add('adresse')->add('telephone');
    }
/*    public function getParent()
    {
        return 'fos_user_registration';
    }
    public function getPrenomClient()
    {
        return 'app_user_registration';
    }
    public function getAdresse()
    {
        return 'app_user_registration';
    }
    public function getTelephone()
    {
        return 'app_user_registration';
    }
*/

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Client'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_client';
    }


}
