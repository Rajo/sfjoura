<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Produit controller.
 *
 * @Route("produit")
 */
class ProduitController extends Controller
{
    /**
     * Lists all produit entities.
     *
     * @Route("/", name="produit_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $produits = $em->getRepository('AppBundle:Produit')->findAll();

        return $this->render('produit/index.html.twig', array(
            'produits' => $produits,
        ));
    }

    /**
     * Creates a new produit entity.
     *
     * @Route("/new", name="produit_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $produit = new Produit();
        $form = $this->createForm('AppBundle\Form\ProduitType', $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();

            return $this->redirectToRoute('produit_show', array('id' => $produit->getId()));
        }

        return $this->render('produit/new.html.twig', array(
            'produit' => $produit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a produit entity.
     *
     * @Route("/{id}", name="produit_show")
     * @Method("GET")
     */
    public function showAction(Produit $produit)
    {
        $deleteForm = $this->createDeleteForm($produit);

        return $this->render('produit/show.html.twig', array(
            'produit' => $produit,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing produit entity.
     *
     * @Route("/{id}/edit", name="produit_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Produit $produit)
    {
        $deleteForm = $this->createDeleteForm($produit);
        $editForm = $this->createForm('AppBundle\Form\ProduitType', $produit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('produit_edit', array('id' => $produit->getId()));
        }

        return $this->render('produit/edit.html.twig', array(
            'produit' => $produit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a produit entity.
     *
     * @Route("/{id}", name="produit_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Produit $produit)
    {
        $form = $this->createDeleteForm($produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($produit);
            $em->flush();
        }

        return $this->redirectToRoute('produit_index');
    }

    /**
     * Creates a form to delete a produit entity.
     *
     * @param Produit $produit The produit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Produit $produit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('produit_delete', array('id' => $produit->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Mise à jour qte stocks.
     *
     * @Route("/{id}/produit_majstk", name="produit_majstk")
     * @Method({"GET", "POST"})
     */
    public function majstockAction(Request $request, Produit $produit)
    {
        $stockactu = $produit->getQte();
        $productId = $produit->getID();

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Produit::class)->find($productId);
        $dummyForm = $this->createFormBuilder($product)
            ->add('qte', TextType::class, array(
                'data'  => '0', 'required'=>0,
            ))
            ->add('add', SubmitType::class, array('label' => 'Mise à jour'))
            ->getForm();

        $dummyForm->handleRequest($request);

        if ($dummyForm->isSubmitted() && $dummyForm->isValid()) {
            $postqte = $product->getQte();
            $qteReal = $stockactu + $postqte;

            $product->setQte($qteReal);

            $em->flush();
            return $this->redirectToRoute('produit_index');
        }

        return $this->render('produit/majstock.html.twig', array(
            'produit' => $product,
            'maj_form' => $dummyForm->createView(),
        ));

    }

    /**
     * Sortie qte stocks.
     *
     * @Route("/{id}/produit_delstk", name="produit_delstk")
     * @Method({"GET", "POST"})
     */
    public function delstockAction(Request $request, Produit $produit)
    {
        $stockactu = $produit->getQte();
        $productId = $produit->getID();

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Produit::class)->find($productId);
        $dummyForm = $this->createFormBuilder($product)
            ->add('qte', TextType::class, array(
                'data'  => '0', 'required'=>0,
            ))
            ->add('add', SubmitType::class, array('label' => 'Mise à jour'))
            ->getForm();

        $dummyForm->handleRequest($request);

        if ($dummyForm->isSubmitted() && $dummyForm->isValid()) {
            $postqte = $product->getQte();
            $qteReal = $stockactu - $postqte;

            $product->setQte($qteReal);

            $em->flush();
            return $this->redirectToRoute('produit_index');
        }

        return $this->render('produit/delstock.html.twig', array(
            'produit' => $product,
            'del_form' => $dummyForm->createView(),
        ));

    }

}
