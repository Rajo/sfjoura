<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fournisseur
 *
 * @ORM\Table(name="fournisseur")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FournisseurRepository")
 */
class Fournisseur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="NomFrns", type="string", length=255, nullable=true)
     */
    private $nomFrns;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomFrns
     *
     * @param string $nomFrns
     *
     * @return Fournisseur
     */
    public function setNomFrns($nomFrns)
    {
        $this->nomFrns = $nomFrns;

        return $this;
    }

    /**
     * Get nomFrns
     *
     * @return string
     */
    public function getNomFrns()
    {
        return $this->nomFrns;
    }

}
