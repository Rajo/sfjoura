<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;



/**
 * CmdProd
 *
 * @ORM\Table(name="cmd_prod")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CmdProdRepository")
 */
class CmdProd
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="IDCmd", type="integer")
     */
    private $iDCmd;

    /**
     * @var int
     *
     * @ORM\Column(name="IDProd", type="integer")
     */
    private $iDProd;

    /**
     * @var int
     *
     * @ORM\Column(name="QteProd", type="integer")
     */
    private $qteProd;

    /**
     * @var int
     *
     * @ORM\Column(name="PrixTotQte", type="smallint")
     */
    private $prixTotQte;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iDCmd
     *
     * @param integer $iDCmd
     *
     * @return CmdProd
     */
    public function setIDCmd($iDCmd)
    {
        $this->iDCmd = $iDCmd;

        return $this;
    }

    /**
     * Get iDCmd
     *
     * @return int
     */
    public function getIDCmd()
    {
        return $this->iDCmd;
    }

    /**
     * Set iDProd
     *
     * @param integer $iDProd
     *
     * @return CmdProd
     */
    public function setIDProd($iDProd)
    {
        $this->iDProd = $iDProd;

        return $this;
    }

    /**
     * Get iDProd
     *
     * @return int
     */
    public function getIDProd()
    {
        return $this->iDProd;
    }

    /**
     * Set qteProd
     *
     * @param integer $qteProd
     *
     * @return CmdProd
     */
    public function setQteProd($qteProd)
    {
        $this->qteProd = $qteProd;

        return $this;
    }

    /**
     * Get qteProd
     *
     * @return int
     */
    public function getQteProd()
    {
        return $this->qteProd;
    }

    /**
     * Set prixTotQte
     *
     * @param integer $prixTotQte
     *
     * @return CmdProd
     */
    public function setPrixTotQte($prixTotQte)
    {
        $this->prixTotQte = $prixTotQte;

        return $this;
    }

    /**
     * Get prixTotQte
     *
     * @return int
     */
    public function getPrixTotQte()
    {
        return $this->prixTotQte;
    }
}
