<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commande
 *
 * @ORM\Table(name="commande")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommandeRepository")
 */
class Commande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NoCmd", type="string", length=255, unique=true)
     */
    private $noCmd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCmd", type="date", nullable=true)
     */
    private $dateCmd;

    /**
     * @var int
     *
     * @ORM\Column(name="Clt", type="integer")
     */
    private $clt;

    /**
     * @var int
     *
     * @ORM\Column(name="PrixTotalCmd", type="smallint")
     */
    private $prixTotalCmd;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set noCmd
     *
     * @param string $noCmd
     *
     * @return Commande
     */
    public function setNoCmd($noCmd)
    {
        $this->noCmd = $noCmd;

        return $this;
    }

    /**
     * Get noCmd
     *
     * @return string
     */
    public function getNoCmd()
    {
        return $this->noCmd;
    }

    /**
     * Set dateCmd
     *
     * @param \DateTime $dateCmd
     *
     * @return Commande
     */
    public function setDateCmd($dateCmd)
    {
        $this->dateCmd = $dateCmd;

        return $this;
    }

    /**
     * Get dateCmd
     *
     * @return \DateTime
     */
    public function getDateCmd()
    {
        return $this->dateCmd;
    }

    /**
     * Set clt
     *
     * @param integer $clt
     *
     * @return Commande
     */
    public function setClt($clt)
    {
        $this->clt = $clt;

        return $this;
    }

    /**
     * Get clt
     *
     * @return int
     */
    public function getClt()
    {
        return $this->clt;
    }

    /**
     * Set prixTotalCmd
     *
     * @param integer $prixTotalCmd
     *
     * @return Commande
     */
    public function setPrixTotalCmd($prixTotalCmd)
    {
        $this->prixTotalCmd = $prixTotalCmd;

        return $this;
    }

    /**
     * Get prixTotalCmd
     *
     * @return int
     */
    public function getPrixTotalCmd()
    {
        return $this->prixTotalCmd;
    }
}
