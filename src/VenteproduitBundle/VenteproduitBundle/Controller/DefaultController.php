<?php

namespace VenteproduitBundle\VenteproduitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('VenteproduitBundleVenteproduitBundle:Default:index.html.twig');
    }
}
